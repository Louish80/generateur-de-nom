import unittest
from Process import correspondance_univers_prenom, correspondance_univers_nom, correspondance_lettre_prenom, correspondance_lettre_nom
from universes import univers

class TestProcess(unittest.TestCase):
    def test_correspondance_univers_prenom(self):
        self.assertEqual(correspondance_univers_prenom(univers, 'Jojo'), 1)
        self.assertEqual(correspondance_univers_prenom(univers, 'Star wars'), 1)
        self.assertEqual(correspondance_univers_prenom(univers, 'Game of thrones'), 1)

    def test_correspondance_univers_nom(self):
        self.assertEqual(correspondance_univers_nom(univers, 'Jojo'), 1)
        self.assertEqual(correspondance_univers_nom(univers, 'Star Wars'), 1)
        self.assertEqual(correspondance_univers_nom(univers, 'Game of thrones'), 1)

    def test_correspondance_lettre_prenom(self):
        self.assertEqual(correspondance_lettre_prenom(univers, 'Jojo', 'S'), 'Notorious')
        self.assertEqual(correspondance_lettre_prenom(univers, 'Star Wars', 'L'), 'Sergeant')
        self.assertEqual(correspondance_lettre_prenom(univers, 'Game of thtones', 'V'), 'Somber')

    def test_correspondance_lettre_nom(self):
        self.assertEqual(correspondance_lettre_nom(univers, 'Jojo', 'E'), 'Dead')
        self.assertEqual(correspondance_lettre_nom(univers, 'Star Wars', 'P'), 'Soldier')
        self.assertEqual(correspondance_lettre_nom(univers, 'Game of thrones', 'Z'), 'Jammer')
