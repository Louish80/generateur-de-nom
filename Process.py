from tkinter import *
from tkinter.ttk import *
from universes import univers, cb_univers 

def correspondance_univers_prenom(univers, univers_demand):
    return univers.get(univers_demand).get('lettre_prenom')

def correspondance_univers_nom(univers, univers_demand):
    return univers.get(univers_demand).get('lettre_nom')

def correspondance_lettre_prenom(univers, univers_demand, lettre):
    return univers.get(univers_demand).get('correspondance_lettre_prenom').get(lettre)

def correspondance_lettre_nom(univers, univers_demand, lettre):
    return univers.get(univers_demand).get('correspondance_lettre_nom').get(lettre)


def clicBouton():
    prenom = _prenom.get()
    nom = _nom.get()
    univers_demand = combobox_univers.get()

    lettre_prenom = correspondance_univers_prenom(univers, univers_demand)
    lettre_nom = correspondance_univers_nom(univers, univers_demand)

    if ( (len(prenom) < abs(lettre_prenom)) | (len(nom) < abs(lettre_nom)) ):
        label_resultat.config(text="Nom ou prénom invalide!")
    else :
        prenom = prenom.upper()
        nom = nom.upper()

        if lettre_prenom > 0 :
            lettre_prenom = prenom[lettre_prenom-1]
        else :
            lettre_prenom = prenom[lettre_prenom]
        
        if lettre_nom > 0 :
            lettre_nom = nom[lettre_nom-1]
        else :
            lettre_nom = nom[lettre_nom]

        if ( (lettre_prenom < 'A') | (lettre_nom > 'Z') | (lettre_prenom < 'A') | (lettre_prenom > 'Z') ):
            label_resultat.config(text="Oops !")
        else:
            new_prenom = correspondance_lettre_prenom(univers, univers_demand, lettre_prenom)
            new_nom = correspondance_lettre_nom(univers, univers_demand, lettre_nom)

            label_resultat.config(text="".join(["Votre Stand est ", new_prenom, " ", new_nom, "."]))
    
if __name__ == "__main__":
    frame=Tk()
    frame.title("Générateur de nom")
    frame.minsize(300, 300)

    frame.rowconfigure(0, weight=1)
    frame.rowconfigure(1, weight=1)
    frame.rowconfigure(2, weight=1)
    frame.rowconfigure(3, weight=1)
    frame.rowconfigure(4, weight=1)

    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)

    label_prenom = Label(frame, text="Prénom :")
    label_prenom.grid(column=0, row=0, sticky="ws", padx=10, pady=10)

    _prenom = Entry(frame)
    _prenom.grid(column=0, row=1, sticky="wn", padx=10, pady=10)

    label_nom = Label(frame, text="Nom :")
    label_nom.grid(column=0, row=2, sticky="ws", padx=10, pady=10)

    _nom = Entry(frame)
    _nom.grid(column=0, row=3, sticky="wn", padx=10, pady=10)

    label_univers = Label(frame, text="Univers :")
    label_univers.grid(column=1, row=0, sticky="wn", padx=10, pady=10)

    combobox_univers = Combobox(
        frame, values=cb_univers, state="readonly")
    combobox_univers.grid(column=1, row=1, sticky="nw", padx=10, pady=10)
    combobox_univers.current(0)

    bouton_generer = Button(frame, text="Générer",
                                command=clicBouton)
    bouton_generer.grid(column=0, row=4, sticky="nw", padx=10, pady=10)

    label_resultat = Label(frame)
    label_resultat.grid(column=1, row=3, rowspan=2, sticky="nw", padx=10, pady=10)

    frame.mainloop()

